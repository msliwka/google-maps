/** This is the JavaScript for "Hawaii holiday" Google Maps API tutorial
 *  View the Bitbucket repository here: https://bitbucket.org/andrewlui/google-maps/
 *  Run the source code here: http://tinyurl.com/alui-mapstutorial
 **/

var hawaiiMap;
var hawaiiLatLng = new google.maps.LatLng(20.75, -157.35);
var oahuLatLng = new google.maps.LatLng(21.44, -157.95);
var honoluluLatLng = new google.maps.LatLng(21.285, -157.834);
var hotelRenewLatLng = new google.maps.LatLng(21.27285,-157.82243);
var alaMoanaLatLng =  new google.maps.LatLng(21.291,-157.843);
var waikikiLatLng = new google.maps.LatLng(21.27292,-157.82385);

// div ids
var steps = {
    intro: "#intro",
    step1: "#step1",
    step2: "#step2",
    step3: "#step3",
    step4: "#step4",
    step5: "#step5",
    step6: "#step6",
    step7: "#step7",
    step8: "#step8",
    finish: "#finish",
    streetviewcanvas: "#streetview-canvas"
};
var test = "#test"


google.maps.event.addDomListener(window, 'load', initialize);

function initialize() {

    // JQuery function to hide the divs for the steps, except for the introduction
    $.each(steps, function(key,value) {
        if (key != "intro") {
            $(value).hide();
        }
    })

    // hide the test div (used for testing methods)
    $("#test").hide();
}

function createHawaiiMap() {
    var hawaiiMapOptions = {
        center: hawaiiLatLng,
        zoom: 8
    };

    hawaiiMap = new google.maps.Map(document.getElementById("map-canvas"),
        hawaiiMapOptions);

/** Include this code to style the map
    var mapStyle = [
    {
     "featureType": "road.local",
     "elementType": "geometry.fill",
     "stylers": [
         { "visibility": "on" },
         { "hue": "#ff0000" },
         { "saturation": 1 },
         { "color": "#a2aec5" }
     ]
    },{
        "featureType": "road.arterial",
        "elementType": "geometry.fill",
        "stylers": [
            { "color": "#a7ac9c" },
            { "saturation": -56 },
            { "lightness": 61 },
            { "gamma": 0.56 },
            { "visibility": "on" }
        ]
    },{
        "featureType": "water",
        "stylers": [
            { "weight": 0.1 },
            { "lightness": 26 },
            { "color": "#97a6d8" }
        ]
    }
    ]
    var styledMap = new google.maps.StyledMapType (mapStyle,{name:"Hawaii Holiday"});
    hawaiiMap.mapTypes.set("hawaiiMapStyle",styledMap);
    hawaiiMap.setMapTypeId("hawaiiMapStyle");
**/
}

function loadHotelRenewMapMarker() {
    // marker options
    var iconHotelRenew = {
        url: "hotel-renew.png",
        size: new google.maps.Size(100, 20),
        origin: new google.maps.Point(0,0),
        anchor: new google.maps.Point(5,5)
    };
    var markerHotelRenewOptions = {
        position: hotelRenewLatLng,
        map: hawaiiMap,
        title: "Hotel Renew",
        icon: iconHotelRenew,
        animation: google.maps.Animation.DROP
    }
    // create the marker
    markerHotelRenew = new google.maps.Marker(markerHotelRenewOptions);
}

function loadAlaMoanaMapMarker() {
    // marker options
    var iconAlaMoana = {
        url: "ala-moana.png",
        size: new google.maps.Size(100, 52),
        origin: new google.maps.Point(0,0),
        anchor: new google.maps.Point(50,25)
    };
    var markerAlaMoanaOptions = {
        position: alaMoanaLatLng,
        map: hawaiiMap,
        title: "Ala Moana",
        icon: iconAlaMoana,
        animation: google.maps.Animation.DROP
    }
    // create the marker
    markerAlaMoana = new google.maps.Marker(markerAlaMoanaOptions);
}

function loadWaikikiMapMarker() {
    // marker options
    var iconWaikiki = {
        url: 'waikiki-beach.png',
        size: new google.maps.Size(100, 52),
        origin: new google.maps.Point(0,0),
        anchor: new google.maps.Point(60,25)
    };
    var markerWaikikiOptions = {
        position: waikikiLatLng,
        map: hawaiiMap,
        title: "Ala Moana",
        icon: iconWaikiki,
        animation: google.maps.Animation.BOUNCE
    }
    // create the marker
    markerWaikiki = new google.maps.Marker(markerWaikikiOptions);
    // add a listener for the marker
    addMarkerListener(markerWaikiki);
}

function addMarkerListener (marker) {
    // add listener to show street view at marker position when marker is clicked
    google.maps.event.addListener(marker, 'click', function () {
        showStreetView(marker.getPosition());
    });
}

function panZoom (latlng,zoom){
    hawaiiMap.panTo(latlng);
    hawaiiMap.setZoom(zoom);
}

function drawOahuCircle () {
    var circleOptions = {
        map: hawaiiMap,
        center: oahuLatLng,
        radius: 50000,
        strokeColor: "#FFFFFF",
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: "FFFFFF",
        fillOpacity: 0.15
    };
    oahuCircle = new google.maps.Circle(circleOptions);
}

function drawHotelShopsRoute() {
    var routeCoordinates = [
        hotelRenewLatLng,
        new google.maps.LatLng(21.2729, -157.8225),
        new google.maps.LatLng(21.274, -157.8213),
        new google.maps.LatLng(21.2778, -157.8245),
        new google.maps.LatLng(21.2797, -157.8264),
        new google.maps.LatLng(21.284, -157.8313),
        new google.maps.LatLng(21.2845, -157.8322),
        new google.maps.LatLng(21.2906, -157.8356),
        new google.maps.LatLng(21.29062, -157.8367),
        new google.maps.LatLng(21.2931, -157.8425),
        alaMoanaLatLng
    ];
    newRoute = new google.maps.Polyline({
        path: routeCoordinates,
        geodesic: true,
        strokeColor: '#FF0000',
        strokeOpacity: 0.5,
        strokeWeight: 4,
        map: hawaiiMap
    });
}

function calcShopsHotelRoute() {
    var directionsService = new google.maps.DirectionsService();
    var directionsDisplay = new google.maps.DirectionsRenderer();

    var request = {
        origin: alaMoanaLatLng,
        destination: new google.maps.LatLng(21.2739, -157.8238),
        travelMode: google.maps.TravelMode.DRIVING
    };

    var directionsDisplayOptions = {
        map:hawaiiMap,
        suppressMarkers:1
    }

    directionsService.route(request, function(response, status) {
        // var div = document.getElementById('beach-directions');
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setOptions(directionsDisplayOptions);
            directionsDisplay.setDirections(response);
            // The following code displays the route as a table of instructions
            // directionsDisplay.setPanel(div);
        }
    });
}

function showStreetView(latLng) {
    var streetViewOptions = {
        position: latLng,
        pov: {
            heading: 260,
            pitch: 0
        }
    };
    newStreetView = new google.maps.StreetViewPanorama(document.getElementById("map-canvas"),streetViewOptions);
}

function showHideDiv (divHidden,divShown) {
    $(divHidden).hide();
    $(divShown).show();
}

function showDivPopup (div) {
    //var div = document.getElementById(elementID).outerHTML;
    console.log(div);
    var popupWindow = window.open("","","width=800,height=500,left=300");
    var winDoc = popupWindow.document;
    winDoc.open();
    winDoc.write("<html>");
    winDoc.write("<head>");
    winDoc.write("<link href='hawaii-holiday-stylesheet.css' rel='stylesheet' type='text/css' />");
    winDoc.write("</head>");
    winDoc.write("<body>");
    winDoc.write(div);
    winDoc.write("</body>");
    winDoc.write("</html>");
    winDoc.close();
}
